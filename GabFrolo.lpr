uses Work_with_matrix6x6;

const MatrixSize=17;
      MatrixRangeDown=-12;
      MatrixRangeUp=12;

type matrixType=array [1..MatrixSize, 1..MatrixSize] of integer;

var matrix: matrixType;
    row1, row2: byte;
    newMatrix: matrix6x6;
    rowM, rowLM, columnM, columnLM: byte;


procedure CreateRandomMatrix(var theMatrix: matrixType);
var i, j: byte;
begin
    for i:=1 to MatrixSize do
        for j:=1 to MatrixSize do
            theMatrix[i,j]:=random(MatrixRangeUp-MatrixRangeDown+1)
            +MatrixRangeDown;
end;

procedure PrintMatrix (theMatrix: matrixType);
var i, j: byte;
begin
    for i:=1 to MatrixSize do
    begin
        for j:=1 to MatrixSize do
        begin
            write(theMatrix[i, j]:4);
        end;
        writeln;
    end;
    writeln;
end;

procedure FindMinMaxRows(var theMatrix: matrixType; var minRow, maxRow: byte);
var i, j: byte;
    min, max: integer;
begin
    min:=theMatrix[1, 1];
    max:=theMatrix[1, 1];
    for i:=1 to MatrixSize do
    begin
        for j:=1 to MatrixSize do
        begin
            if theMatrix[i, j]<=min then
            begin
                min:=theMatrix[i, j];
                minRow:=i;
            end
            else if theMatrix[i, j]>max then
                 begin
                     max:=theMatrix[i, j];
                     maxRow:=i;
                 end;
        end;
        if (max=MatrixRangeUp) and (min=MatrixRangeDown) and (minRow<>maxRow) then
        begin
             break;
        end;
    end;
end;

procedure SwapRows (var theMatrix: matrixType; minRow, maxRow: byte);
var i: byte;
    interArray: array [1..MatrixSize] of integer;
begin
    for i:=1 to MatrixSize do
    begin
        interArray[i]:=theMatrix[minRow, i];
        theMatrix[minRow, i]:=theMatrix[maxRow, i];
        theMatrix[maxRow, i]:=interArray[i];
    end;
end;

procedure CheckRiseCondition(theMatrix: matrixType);
var i, j: byte;
    fail: boolean;
begin
    for i:=1 to MatrixSize do
    begin
        fail:=false;
        for j:=1 to MatrixSize-1 do
            if theMatrix[i, j+1]<=theMatrix[i, j] then
            begin
                fail:=true;
                break;
            end;
        if not (fail) then write(i:4);
    end;
end;

begin
    randomize;
    CreateRandomMatrix(matrix);
    PrintMatrix(matrix);
    row1:=1;
    row2:=1;
    FindMinMaxRows(matrix, row1, row2);
    writeln('Row with min value element: ', row1);

    writeln('Row with max value element: ', row2);
    writeln;
    SwapRows(matrix, row1, row2);
    PrintMatrix(matrix);
    writeln('These rows are relevant: ');
    CheckRiseCondition(matrix);
 	writeln;
    writeln('Laboratory work 2');
    CreateSquareMatrix(newMatrix, -17, 20);
    ShowMatrix(newMatrix);
    writeln('Max element of main diagonal');
    writeln('value: ', FindMaxElementMainDiag(newMatrix, rowM, columnM));
    writeln('row: ', rowM);
    writeln('column: ', columnM);
    writeln('Max element of lower then main diagonal');
    writeln('value: ', FindMaxElementLowerMainDiag(newMatrix, rowLM, columnLM));
    writeln('row: ', rowLM);
    writeln('column: ', columnLM);
    ReplaceMaxFromLowerMainToMainDiag(newMatrix, rowLM, columnLM, rowM, columnM);
    writeln('Matrix after replacing max element from lower then main diag to main diag');
    ShowMatrix(newMatrix);
    readln;
end.
