unit work_with_matrix6x6;

{$mode objfpc}{$H+}

interface
const Size = 6;

type matrix6x6 = array [1..Size, 1..Size] of integer;

procedure CreateSquareMatrix(var matrix: matrix6x6; rangeDown, rangeUp: integer);
function FindMaxElementMainDiag(var matrix: matrix6x6; var posRow, posColumn: byte): integer;
function FindMaxElementLowerMainDiag(var matrix: matrix6x6; var posRow, posColumn: byte): integer;
procedure ReplaceMaxFromLowerMainToMainDiag(var matrix: matrix6x6; rowLM, columnLM, rowM, columnM: byte);
procedure ShowMatrix(var matrix: matrix6x6);

implementation

procedure CreateSquareMatrix(var matrix: matrix6x6; rangeDown, rangeUp: integer);
var i, j: byte;
begin
    randomize;
    for i := 1 to Size do
    begin
        for j := 1 to Size do
        begin
            matrix[i, j] := random(rangeUp - rangeDown + 1) + rangeDown;
        end;
    end;
end;

function FindMaxElementMainDiag(var matrix: matrix6x6; var posRow, posColumn: byte): integer;
var i, j: byte;
    max: integer;
begin
    max:=matrix[0][0];
    for i:=1 to Size do
        for j:=1 to SIze do
            if (i=j) then
            begin
                 if (matrix[i,j]>max) then
                 begin
                      max:=matrix[i,j];
                      posRow:=i;
                      posColumn:=j;
                 end;
            end;
    FindMaxElementMainDiag:=max;
end;

function FindMaxElementLowerMainDiag(var matrix: matrix6x6; var posRow, posColumn: byte): integer;
var i, j: byte;
    max: integer;
begin
    max:=matrix[0][0];
    for i:=1 to Size do
        for j:=1 to SIze do
            if (i>j) then
            begin
                 if (matrix[i,j]>max) then
                 begin
                      max:=matrix[i,j];
                      posRow:=i;
                      posColumn:=j;
                 end;
            end;
    FindMaxElementLowerMainDiag:=max;
end;

procedure ReplaceMaxFromLowerMainToMainDiag(var matrix: matrix6x6; rowLM, columnLM, rowM, columnM: byte);
begin
    matrix[rowM][columnM]:=matrix[rowLM][columnLM];
end;

procedure ShowMatrix(var matrix: matrix6x6);
var i, j: byte;
begin
    for i := 1 to Size do
    begin
        for j := 1 to Size do
        begin
            write(matrix[i, j]:5);
        end;
        writeln;
    end;
    writeln;
end;

end.

